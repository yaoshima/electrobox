<?php 
class ProductModel extends CI_Model
{

   public function __construct()
   {
       parent::__construct();
   }

   public function index(){
      return $this->db->get('products')->result_array();
   }

   public function show($id){
      return $this->db->get_where('products', ['id' => $id])->row_array();
   }

   public function getByCategory($categoryId){
      return $this->db->get_where('products', ['category_id' => $categoryId])->result_array();
   }

   public function getLikeName($name){
      $this->db->like('name', $name);
      return $this->db->get('products')->result_array();
   }

   // public function insert($data){
   //    $this->db->insert('products',$data);
   //    return $this->db->affected_rows() > 0;
   // }

   // public function update($id,$data){
   //    $this->db->update('products', $data, ['id' => $id]);
   //    return $this->db->affected_rows() > 0;
   // }


   
}