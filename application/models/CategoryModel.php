<?php 
class CategoryModel extends CI_Model
{

   public function __construct()
   {
       parent::__construct();
   }


   public function index(){
      return $this->db->get('categories')->result_array();
   }

   public function getByName($name){
      return $this->db->get_where('categories', ['name' => $name])->row_array();
   }


}