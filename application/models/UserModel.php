<?php 
class UserModel extends CI_Model
{

   public function __construct()
   {
       parent::__construct();
   }

   public function authenticate($data){
      return $this->db->get_where('users', ['email' => $data['email'], 'password' => $data['password'] ])->row_array();
   }

   public function register($data){
      $response =[];

      if($this->getUserByEmail($data['email'])){
         $response['status'] = false;
         $response['message'] = "Email already been used!";
         return $response;
      }

      $this->db->insert('users',$data);
      if($this->db->affected_rows() > 0){
         $data['id'] = $this->db->insert_id();
         $response['status'] = true;
         $response['currentUser'] = $data;
         return $response;
      }
      else{
         $response['status'] = false;
         $response['message'] = $this->db->_error_message();
      }
      
   }

   public function getUserByEmail($email){
      return $this->db->get_where('users', ['email' => $email ])->row_array();
   }


   
}