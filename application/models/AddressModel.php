<?php 
class AddressModel extends CI_Model
{

   public function __construct()
   {
       parent::__construct();
   }


   public function insert($data){
      $this->db->insert('addresses',$data);
      return $this->db->affected_rows() > 0;
   }

   public function userAddress($userId){
      return $this->db->get_where('addresses', ['user_id' => $userId])->result_array();
   }

   public function delete($userId, $id){
      $this->db->delete('addresses', ['id' => $id, 'user_id' => $userId]);   
      return $this->db->affected_rows() > 0;
   }

   public function update($userId, $data){
      $id = $data['id'];
      unset($data['id']);

      $this->db->update('addresses', $data, ['id' => $id, 'user_id' => $userId]);
      return $this->db->affected_rows() > 0;

   }



   
}