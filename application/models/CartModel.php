<?php 
class CartModel extends CI_Model
{

   public function __construct()
   {
       parent::__construct();
   }


   public function insert($data){

      $this->db->insert('cart',$data);
      if( $this->db->affected_rows() > 0 ){
         $id = $this->db->insert_id();
         return [
            'status' => true,
            'id' => $id
         ];
      }
      else{
         return [
            'status' => false,
            'message' => $this->db->error()
         ];
      }
   }

   public function userCart($userId){
      $this->db->select('cart.id, product_id, qty, subtotal, name, price, image');
      $this->db->from('cart');
      $this->db->join('products', 'products.id = cart.product_id');
      $result = $this->db->get()->result_array();

      //echo "<pre>" ,print_r($result), "</pre>";

      $response = [];
      foreach ($result as $row ) {
         $response[] = [
            'id' => $row['id'],
            'product' => [
               'id' => $row['product_id'],
               'name' => $row['name'],
               'price' => $row['price'],
               'image' => $row['image']
            ],
            'qty' => $row['qty'],
            'subtotal' => $row['subtotal']
         ];
      }
      return $response;
   }

   public function update($data){
      $id = $data['id'];
      unset($data['id']);

      $this->db->update('cart', $data, ['id' => $id]);

      if( $this->db->affected_rows() > 0 ){
         return [
            'status' => true
         ];
      }
      else{
         return [
            'status' => false,
            'message' => $this->db->error()
         ];
      }

   }

   public function delete($data){
      $id = $data['id'];

      $this->db->delete('cart', ['id' => $id]);
      if( $this->db->affected_rows() > 0 ){
         return [
            'status' => true
         ];
      }
      else{
         return [
            'status' => false,
            'message' => $this->db->error()
         ];
      }
   }






   
}