<?php 
class TransactionModel extends CI_Model
{

   public function __construct()
   {
       parent::__construct();
   }

   public function userTransaction($userId){
      $q = "
         SELECT t.id as \"id\", p.id as \"product_id\", name, image, concat (address, ', ' , province, ', ', city, ', ', sub_district , ', ', postal_code  ) as \"shipping_address\", price, qty, subtotal, item_count , shipping_fee, total, payment_method, shipping_method, status, DATE_FORMAT(t.created_at,'%d %M %Y') as \"created_at\"
         FROM transactions as t
         JOIN transactions_detail td on td.transaction_id = t.id
         JOIN products as p on p.id = td.product_id
         JOIN addresses as a on a.id = t.address_id
         WHERE t.user_id = $userId
      ";

      $result = $this->db->query($q)->result_array();
      //echo "<pre>",print_r($result),"</pre>";
      //var_dump($result);

      $resultLength = count($result);
      if ($resultLength == 0) return [];

      // $singleResult = $result[0];
      // $response = [
      //    'id' => $singleResult['id'],
      //    'products' => [],
      //    'shipping_address' => $singleResult['shipping_address'],
      //    'payment_method' => $singleResult['payment_method'],
      //    'shipping_method' => $singleResult['shipping_method'],
      //    'status' => $singleResult['status'],
      //    'shipping_fee' => $singleResult['shipping_fee'],
      //    'item_count' => $singleResult['item_count'],
      //    'total' => $singleResult['total'],
      //    'created_at' => $singleResult['created_at']
      // ];
      // foreach ($result as $row ) {
      //    $response['products'][] = [
      //       'id' => $row['product_id'],
      //       'name' => $row['name'],
      //       'price' => $row['price'],
      //       'image' => $row['image'],
      //       'qty' => $row['qty']
      //    ];
      // }
      $response = [];

      $firstRow = $result[0];
      //echo "<pre>",print_r($result),"</pre>";
      $currentId = $firstRow['id'];
      $products = [];
      $transaction = [];


      $idx = 1;
      foreach ($result as $row) {
         

         if($currentId != $row['id']){
            $transaction['products'] = $products;
            $response[] = $transaction;
            $products = [];
            $transaction = [];
         }

         if(count($products) == 0){
            $transaction = [
               'id' => $row['id'],
               'products' => [],
               'shipping_address' => $row['shipping_address'],
               'payment_method' => $row['payment_method'],
               'shipping_method' => $row['shipping_method'],
               'status' => $row['status'],
               'shipping_fee' => $row['shipping_fee'],
               'item_count' => $row['item_count'],
               'total' => $row['total'],
               'created_at' => $row['created_at']
            ];
         }
         $products[] = [
            'id' => $row['product_id'],
            'name' => $row['name'],
            'price' => $row['price'],
            'image' => $row['image'],
            'qty' => $row['qty'],
            'subtotal' => $row['subtotal']
         ];

         if($idx == $resultLength){
            $transaction['products'] = $products;
            $response[] = $transaction;
         }

         $currentId = $row['id'];
         $idx++;


         // if($currentId == $result['id']){
         //    if(count($products) == 0){
         //       $transaction = [
         //          'id' => $row['id'],
         //          'products' => [],
         //          'shipping_address' => $row['shipping_address'],
         //          'payment_method' => $row['payment_method'],
         //          'shipping_method' => $row['shipping_method'],
         //          'status' => $row['status'],
         //          'shipping_fee' => $row['shipping_fee'],
         //          'item_count' => $row['item_count'],
         //          'total' => $row['total'],
         //          'created_at' => $row['created_at']
         //       ] 
         //    }
         //    $products[] = [
         //       'id' => $row['product_id'],
         //       'name' => $row['name'],
         //       'price' => $row['price'],
         //       'image' => $row['image'],
         //       'qty' => $row['qty']
         //    ]
         // }
         // else{
         //    $transaction['products'] = $products;
         //    $response[] = $transaction;
         //    $products = [];
         //    $transaction = [];


         // }

         // $currentId = $row['id'];

      }

      return $response;
   }

   public function storeRegisteredUserTx($data){
      // get the product that user buy    
      $this->db->select('product_id, qty, subtotal');
      $userCartItem = $this->db->get_where('cart', ['user_id' => $data['user_id']])->result_array();
      if (count($userCartItem) == 0) return;

      // insert to transaction table
      $this->db->insert('transactions',$data);
      if($this->db->affected_rows() < 1) return;

      $transactionId = $this->db->insert_id();
      $cartWithTxId = [];
      foreach ($userCartItem as $item) {
         $item['transaction_id'] = $transactionId;
         $cartWithTxId[] = $item;
      }

      $this->db->insert_batch('transactions_detail', $cartWithTxId); 

      // delete user cart
      $this->db->delete('cart', ['user_id' => $data['user_id']]);

      if( $this->db->affected_rows() > 0 ){
         return [
            'status' => true,
            'id' => $transactionId
         ];
      }
      else{
         return [
            'status' => false,
            'message' => $this->db->error()
         ];
      }
        
   }

   public function storeUnRegisteredUserTx($data){
      

      // foreach ($products as $product) {
      //    # code...
      // }

      $this->db->insert('addresses',$data['shipping_detail']);
      if ($this->db->affected_rows() < 1) return;

      $addressId = $this->db->insert_id();
      unset($data['shipping_detail']);
      $products = $data['products'];      
      unset($data['products']);

      $data['address_id'] = $addressId;
      // insert to transaction table
      $this->db->insert('transactions',$data);
      if($this->db->affected_rows() < 1) return;

      $transactionId = $this->db->insert_id();
      $cleanProdcts = [];
      foreach ($products as $product) {
         $product['transaction_id'] = $transactionId;
         $product["product_id"] = $product["id"];
         unset($product["id"]);
         $cleanProdcts[] = $product;
      }

      $this->db->insert_batch('transactions_detail', $cleanProdcts);

      if( $this->db->affected_rows() > 0 ){
         return [
            'status' => true,
            'id' => $transactionId
         ];
      }
      else{
         return [
            'status' => false,
            'message' => $this->db->error()
         ];
      }


   }



   
}