<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Electrobox</title>
    <link rel="stylesheet" href="<?= base_url('public/css/app.css') ?>" >

</head>
<body>

	<div id="app">
		<?= $topNav ?>
		<search-modal ref="searchModal"></search-modal>
		<div class="flex container mx-auto mb-4">
			<?= $sideNav ?>

			<main class="flex-1">
				<!-- video highlight -->
				<div class="videoWrapper">
				    <!-- Copy & Pasted from YouTube -->
				    <iframe width="560" height="315" src="https://www.youtube.com/embed/4_5YIbseUAI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				
				<!-- shop -->
				<section class="pt-16">
					<header class="section-header">
						<span class="line"></span>
						<span>SHOP</span>
					</header>
					
					<!-- products -->
					<div class="row pt-16">
					  <div class="w-1/4">
					    <div class="bg-grey-light h-64"></div>
					    <p class="text-center mt-2">Lorem</p>
					  </div>
					  <div class="w-1/4">
					    <div class="bg-grey h-64"></div>
					    <p class="text-center mt-2">Lorem</p>
					  </div>
					  <div class="w-1/4">
					    <div class="bg-grey-light h-64"></div>
					    <p class="text-center mt-2">Lorem</p>
					  </div>
					  <div class="w-1/4">
					    <div class="bg-grey-light h-64"></div>
					    <p class="text-center mt-2">Lorem</p>
					  </div>
					</div>

					<!-- button -->
					<div class="text-center pt-16">
						<button class="primary-btn">View All Categories</button>
					</div>
				</section>

				<!-- stories -->
				<section class="pt-16">
					<header class="section-header">
						<span class="line"></span>
						<span>STORIES</span>
					</header>
					
					<!-- stories -->
					<div class="row pt-16">
					  <div class="flex flex-row-reverse w-1/2 relative">
					    <div class="w-3/5 bg-grey-light h-80"></div>
					    <div class="absolute" style="top: 50%; right: 20%">
					    	<p class="text-2xl font-normal truncate w-64">Lorem ipsum</p>
					    	<span class="text-grey-darker text-sm border-b border-black">Read More</span>
					    </div>
					  </div>
					  <div class="flex flex-row-reverse w-1/2 relative">
					    <div class="w-3/5 bg-grey-light h-80"></div>
					    <div class="absolute" style="top: 50%; right: 20%">
					    	<p class="text-2xl font-normal truncate w-64">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
					    	<span class="text-grey-darker text-sm border-b border-black">Read More</span>
					    </div>
					  </div>
					</div>

					<!-- button -->
					<div class="text-center pt-16">
						<button class="primary-btn">View All Stories</button>
					</div>
				</section>
			</main>
		</div>
	</div>
	
	

	<?= $newsletter ?>

	<?= $footer ?>
	<script src="<?= base_url('public/js/app.js') ?>"></script>
</body>
</html>