<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Electrobox</title>
    <link rel="stylesheet" href="<?= base_url('public/css/app.css') ?>" >

</head>
<body>
	<div id="app">
		<?= $topNav ?>
		<search-modal ref="searchModal"></search-modal>
		<div class="container mx-auto mb-4">

			<main>

				<div v-if="isCartEmpty" class="my-12 text-center">
					<img src="<?= base_url('public/img/empty_cart.png' ) ?>" alt="">
					<p class="mt-1 font-semibold">Your cart is currently empty</p>
					<div class="w-1/4 mx-auto my-16">
						<a href="<?= site_url('/') ?>" class="block full-btn secondary-btn link-btn">CONTINUE SHOPPING</a>
					</div>
				</div>

				<div v-else>
					<cart></cart>
				</div>
			</main>

			
		</div>
	</div>
	
	

	

	<?= $newsletter ?>

	<?= $footer ?>
	<script src="<?= base_url('public/js/app.js') ?>"></script>
</body>
</html>