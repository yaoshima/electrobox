<!DOCTYPE html>
<html>
<head>
	<script>
		if(localStorage.getItem('customerInfo')){
			try {
			  let customerInfo = JSON.parse(localStorage.getItem('customerInfo'));
			  if(!customerInfo.payment_method){
			  	window.location.replace (  window.location.origin + '/index.php/checkout/payment' )
			  }
			} catch(e) {
			  localStorage.removeItem('customerInfo');
			}
			
		}
		else{
			window.location.replace (  window.location.origin + '/index.php/checkout/information' )
		}
		
	</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Electrobox</title>
    <link rel="stylesheet" href="<?= base_url('public/css/app.css') ?>" >

    <style>
    	.material-icons.md-18 { font-size: 18px; }
    	.material-icons.md-24 { font-size: 24px; }
    	.material-icons.md-36 { font-size: 36px; }
    	.material-icons.md-48 { font-size: 48px; }
    </style>
</head>
<body>

	<main id="app" class="flex">
		<div class="w-3/5 pt-16 pl-32 pr-12">
			<!-- brand -->
			<a href="<?= site_url('/') ?>" class="text-2xl uppercase no-underline">Electrobox</a>

			<div class="text-center p-4 my-12 border rounded bg-grey-lightest">
				<i class="material-icons md-48">
				check_circle_outline
				</i>
				<p>Thank you {{customerInfoGeneral.firstname}}!</p>
				<p>Your order number is #{{customerInfoGeneral.transaction_id}}</p>
			</div>

			<!-- order update -->
			<div class="p-4 my-12 border rounded">
				<p class="text-xl mb-3">Order updates</p>
				<p>You’ll get shipping and delivery updates by email.</p>
			</div>

			


			<!-- summary of user information -->
			<div class="flex flex-wrap px-4 my-12 border rounded justify-between">
				<div class="w-full  mt-3 text-xl" >
					<p>Customer information</p>
				</div>
				<div class="w-2/5  my-3 ">
					<p class="font-semibold leading-normal">Contact information</p>
					<p>{{customerInfoGeneral.email}}</p>
				</div>
				<div class="w-2/5  my-3 ">
					<p class="font-semibold leading-normal">Payment method</p>
					<p>{{customerInfoGeneral.payment_method}}</p>
				</div>
				<div class="w-2/5  my-3 ">
					<p class="font-semibold leading-normal">Shipping address</p>
					<p>{{customerInfoGeneral.shipping_address}}</p>
				</div>
				<div class="w-2/5  my-2 ">
					<p class="font-semibold leading-normal">Shipping method</p>
					<p>
						{{customerInfoGeneral.shipping_method}}
					</p>
				</div>
			</div>



			<div class="my-12 ">
				
				
				<a @click="clearLocalStorage" href="<?= site_url('/') ?>" class="checkout-btn link-btn">Continue shopping</a>
				
			</div>
			
			<?= $footer ?>
			
		</div>

		<?= $checkoutItems ?>
	</main>

	<script src="<?= base_url('public/js/app.js') ?>"></script>
</body>
</html>