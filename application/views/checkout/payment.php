<!DOCTYPE html>
<html>
<head>
	<script>
		if(localStorage.getItem('customerInfo')){
			try {
			  let customerInfo = JSON.parse(localStorage.getItem('customerInfo'));
			  if(!customerInfo.shipping_method){
			  	window.location.replace (  window.location.origin + '/index.php/checkout/shipping' )
			  }
			} catch(e) {
			  localStorage.removeItem('customerInfo');
			}
			
		}
		else{
			window.location.replace (  window.location.origin + '/index.php/checkout/information' )
		}
		
	</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Electrobox</title>
    <link rel="stylesheet" href="<?= base_url('public/css/app.css') ?>" >
</head>
<body>

	<main id="app" class="flex">
		<div class="w-3/5 pt-16 pl-32 pr-12">
			<?= $header ?>

			<!-- summary of user information -->
			<ul class="list-reset my-12 border rounded">
				<li class="flex border-b p-4">
					<p class="w-1/6 text-grey-darker">Contact</p>
					<p class="flex-1">{{customerInfoGeneral.email}}</p>
					<a href="#" class="pl-2 no-underline">Change</a>
				</li>
				<li class="flex p-4 border-b">
					<p class="w-1/6 text-grey-darker">Ship to</p>
					<p class="flex-1">
						{{customerInfoGeneral.shipping_address}}
					</p>
					<a href="#" class="pl-2 no-underline">Change</a>
				</li>
				<li class="flex p-4 border-b">
					<p class="w-1/6 text-grey-darker">Method</p>
					<p class="flex-1">
						{{customerInfoGeneral.shipping_method}}
					</p>
				</li>
			</ul>

			<payment-form :current_user="currentUser"></payment-form>
			
			<?= $footer ?>
			
		</div>

		<?= $checkoutItems ?>
	</main>

	<script src="<?= base_url('public/js/app.js') ?>"></script>
</body>
</html>