<!DOCTYPE html>
<html>
<head>
	<script>
		// if(!localStorage.getItem('cartItems')){
		// 	window.location.replace (  window.location.origin + '/' )
		// }
		
	</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Electrobox</title>
    <link rel="stylesheet" href="<?= base_url('public/css/app.css') ?>" >
	<style>
		.label-floating input:not(:placeholder-shown) {
		  padding-top: 1.4rem;
		}
		.label-floating input:not(:placeholder-shown) ~ label {
		  font-size: 0.6rem;
		  transition: all 0.2s ease-in-out;
		  color: #b8c2cc;
		}
	</style>
</head>
<body>
	<div id="app">
		<general-modal ref="no_address_modal"></general-modal>
		<main class="flex">
			<div class="w-3/5 pt-16 pl-32 pr-12">
				<?= $header ?>

				<information-form :current_user="currentUser"></information-form>
				
				<?= $footer ?>
				
			</div>

			<?= $checkoutItems ?>
		</main>
	</div>
	

	<script src="<?= base_url('public/js/app.js') ?>"></script>
</body>
</html>