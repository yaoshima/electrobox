<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Electrobox</title>
    <link rel="stylesheet" href="<?= base_url('public/css/app.css') ?>" >

    <style>

    	.material-icons{
    		transition: 0.3s ease;
    	}
		.active-icon{
			transform: rotate(45deg);
		}



    </style>
</head>
<body>
	
	

	<div id="app">
		<?= $topNav ?>
		<overlay-cart ref="overlayCart"></overlay-cart>
		<search-modal ref="searchModal"></search-modal>
		<div class="flex container mx-auto mb-4">
			<?= $sideNav ?>

			
			<main class="flex-1 mb-24">
				<!-- main product -->
				<div class="lg:row">
					<div class="lg:w-1/2">
						<div class="w-4/5">
							<!-- images -->
							<img class="shadow" src="<?= base_url('public/img/'. $product['image'] ) ?>" alt="">
							<!-- <div class="h-80 bg-teal"></div> -->
						</div>
					</div>

					<div class="lg:w-1/2">
						<div class="w-4/5">
							<header class="product-header">
								<!-- product name -->
								<p><?= $product['name'] ?></p>
								<!-- price -->
								<p>IDR <?= number_format($product['price']) ?></p>
							</header>
							<div class="product-main">
								<p>
									<?= $product['description'] ?>
								</p>
							</div>

							<div class="mt-6">
								<button @click="addToCart(<?= html_escape(json_encode($product)) ?>)" class="secondary-btn full-btn">ADD TO CART</button>
							</div>

							<!-- other details -->
							
							<accordion>
								<accordion-item>
									<template slot="title">
										PRODUCT DETAILS
									</template>
									<template slot="content">
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis ex vitae metus scelerisque dapibus quis at velit. Cras vitae diam placerat, lobortis arcu nec, imperdiet augue. Integer at tellus lectus. Donec eget urna congue, interdum enim ac, consectetur tellus. Vestibulum at turpis tempus, tincidunt libero sit amet, egestas purus.
									</template>
								</accordion-item>

								<accordion-item>
									<template slot="title">
										SPECIFICATION
									</template>
									<template slot="content">
										<?= $product['specification'] ?>
									</template>
								</accordion-item>

								<accordion-item>
									<template slot="title">
										SHIPPING &amp; RETURN
									</template>
									<template slot="content">
										<p class="mb-2 font-semibold">Pengembalian dan Penukaran</p>
										<p class="mb-2">Demi mengutamakan pelayanan yang maksimal kepada pembeli di Electrobox, kami menyediakan fasilitas Pengembalian Barang (Return) dan Penukaran Barang (Exchange). Pembeli dapat menggunakan fasilitas ini apabila:</p>
										<ul>
											<li>Menukar dengan barang lain dengan harga yang sama atau lebih tinggi</li>
											<li>Terdapat cacat pada barang yang diterima</li>
										</ul>
									</template>
								</accordion-item>
							</accordion>
							

							<!-- <div class="accordion mt-6">
								<div class="accordion-item">
									<header>
										<p>PRODUCT DETAILS</p>
										<i class="material-icons">add</i>
									</header>
									<div>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis ex vitae metus scelerisque dapibus quis at velit. Cras vitae diam placerat, lobortis arcu nec, imperdiet augue. Integer at tellus lectus. Donec eget urna congue, interdum enim ac, consectetur tellus. Vestibulum at turpis tempus, tincidunt libero sit amet, egestas purus.
										</p>
									</div>
								</div>
								<div class="accordion-item">
									<header>
										<p>SPECIFICATION</p>
										<i class="material-icons">add</i>
									</header>
								</div>
								<div class="accordion-item">
									<header>
										<p>SHIPPING &amp; RETURN</p>
										<i class="material-icons">add</i>
									</header>
								</div>
							</div> -->
							
							
						</div>
					</div>
				</div>

				<!-- recommended products -->
				<section class="pt-16">
					<header class="section-header">
						<span class="line"></span>
						<span>RECOMMENDED</span>
					</header>
					
					<!-- products -->
					<div class="row pt-16">
					  <div class="w-1/4">
					    <div class="bg-grey-light h-64"></div>
					    <p class="text-center mt-2">Lorem</p>
					  </div>
					  <div class="w-1/4">
					    <div class="bg-grey h-64"></div>
					    <p class="text-center mt-2">Lorem</p>
					  </div>
					  <div class="w-1/4">
					    <div class="bg-grey-light h-64"></div>
					    <p class="text-center mt-2">Lorem</p>
					  </div>
					  <div class="w-1/4">
					    <div class="bg-grey-light h-64"></div>
					    <p class="text-center mt-2">Lorem</p>
					  </div>
					</div>
				</section>
			</main>
		</div>
	</div>

	

	

	

	<?= $newsletter ?>

	<?= $footer ?>
	<script src="<?= base_url('public/js/app.js') ?>"></script>
</body>
</html>