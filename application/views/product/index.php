<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Electrobox</title>
    <link rel="stylesheet" href="<?= base_url('public/css/app.css') ?>" >
	<style>
		.filter span {
		  position: relative;
		}

		.filter span:after{
			content: '';
			display: block;
			margin-top:  0.5rem;
			border-top: 1px solid red;
			width: 0;
			position: absolute;
			left: 0;
			-webkit-transition: 0.5s ease;
			transition: 0.5s ease;
		}

		.filter-hover:hover:after{
			width: 100%; 
		}

		.filter-actv{
			border-bottom: 1px solid red;
		}
	</style>
</head>
<body>

	<div id="app">
		<?= $topNav ?>
		<search-modal ref="searchModal"></search-modal>
		<div class="flex container mx-auto mb-4">
			<?= $sideNavHasChildren ?>

			<main class="flex-1">
				<header>
					<!-- title -->
					<p class="text-2xl font-medium">Shop</p>
					<!-- various filter -->
					<div class="filter">
						<span>Filter</span>
						<span class="<?= $currentCategory == 'ALL' ? 'filter-actv' : 'filter-hover'?>"><a href="<?= site_url('/product') ?>">ALL</a></span>
						<?php foreach($categories as $category) : ?>
							<span class="<?= $currentCategory == $category['name'] ? 'filter-actv' : 'filter-hover' ?>"><a href="<?= site_url('/product?category=' .$category['name'] ) ?>"><?= $category['name'] ?></a></span>
						<?php endforeach ?>
						<!-- <span class="filter-actv"><a href="#">Battery</a></span>
						<span class="filter-hover"><a href="#">Tripod</a></span>
						<span class="filter-hover"><a href="#">Camera</a></span>
						<span class="filter-hover"><a href="#">Misc</a></span> -->
					</div>
				</header>
				
				<!-- products -->
				<div class="row mt-8">
					<?php if($products) : ?>
						<?php foreach($products as $product) : ?>
							<div class="w-1/3 my-4">
								<a class="no-underline text-grey-darkest" href="<?= site_url('/product/'.$product['id']) ?>">
									
									<img class="shadow" src="<?= base_url('public/img/'. $product['image'] ) ?>" alt="">
									
									<div class="product-short-desc">
										<p><?= $product['name'] ?></p>
										<p>IDR <?= number_format($product['price']) ?></p>
									</div>
								</a>
							</div>
						<?php endforeach ?>
					<?php else : ?>
						<div class="w-full my-4">
							<p>No Products</p>
						</div>
					<?php endif ?>
					
				</div>
			</main>
		</div>
	</div>
	
	

	<?= $newsletter ?>

	<?= $footer ?>
	<script src="<?= base_url('public/js/app.js') ?>"></script>
</body>
</html>