
<!DOCTYPE html>
<html>
<head>
	<script>
		localStorage.removeItem('customerInfo')
        localStorage.removeItem('cartItems')
	</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Electrobox</title>
    <link rel="stylesheet" href="<?= base_url('public/css/app.css') ?>" >

</head>
<body>
	
	

	<div id="app">
		<?= $topNav ?>
		<search-modal ref="searchModal"></search-modal>
		<address-modal ref="addressModal"></address-modal>

		<div class="flex container mx-auto mb-4">
			<?= $sideNav ?>

			<main class="flex-1">
				<div class="flex flex-wrap -mx-8">
					<div class="px-8 w-full mb-10">
						<p class="mb-6 font-medium text-2xl">My account</p>
						<p>Welcome back, <?= $currentUser['firstname'] ." ". $currentUser['lastname']?> !</p>
					</div>

					<?php if($successMsg) : ?>
						<disappear>
							<div class="px-8 mb-10 w-full">
								<div class="bg-blue-lightest border-t border-b border-blue text-blue-dark px-4 py-3" >
								  <p class="font-bold capitalize">Succeed</p>
								  <p class="text-sm capitalize"><?= $successMsg ?></p>
								</div>
							</div>
						</disappear>
					<?php endif ?>

					<?php if($failedMsg) : ?>
						<disappear>
							<div class="px-8 mb-10 w-full">
								<div class="bg-red-lightest border-t border-b border-red text-red-dark px-4 py-3" >
								  <p class="font-bold capitalize">Failed</p>
								  <p class="text-sm capitalize"><?= $failedMsg ?></p>
								</div>
							</div>
						</disappear>
					<?php endif ?>
					
					<div class="px-8 w-2/3 ">
						<header class="border-b pb-2 mb-6">
							<p class="text-grey-darker">My orders</p>
						</header>

						<order-history :current_user="currentUser"></order-history>

						
					</div>
					<div class="px-8 w-1/3 ">
						<header class="border-b pb-2 mb-6">
							<p class="text-grey-darker">My addresses</p>
						</header>
						<?php if($addresses) : ?>
							<ul class="list-reset -mt-8 overflow-auto">
								<?php foreach($addresses as $address) : ?>
									<li class="leading-normal py-8 border-b">
										<p class="font-semibold"><?= $address['firstname'] . " " . $address['lastname'] ?></p>
										<p>
											<?= $address['address'] . " " . $address['province'] .  " " . $address['city'] .  " " . $address['sub_district'] .  " " . $address['postal_code'] ?>
										</p>
										<p><?= $address['phone'] ?></p>
										<div class="flex mt-1 ">
											<a @click.prevent="openAddressModal($event,<?= html_escape(json_encode($address)) ?>)" class="w-1/5 no-underline text-black" href="#">Edit</a>
											<a class="w-1/5 no-underline text-black" href="<?= site_url('/address/delete/'.$address['id']) ?>">Delete</a>
										</div>
									</li>
								<?php endforeach  ?>	
							</ul>
						<?php else : ?>	
							<p class="text-grey-darker mb-6">
								No addresses are currently saved
							</p>
							
						<?php endif  ?>	

						<div>
							<button @click="openAddressModal" class="secondary-btn full-btn">MANAGE ADDRESSES</button>
						</div>
						
						
					</div>
				</div>
			</main>
		</div>
	</div>

	

	<?= $newsletter ?>

	<?= $footer ?>
	<script src="<?= base_url('public/js/app.js') ?>"></script>
</body>
</html>