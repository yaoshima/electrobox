<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Electrobox</title>
    <link rel="stylesheet" href="<?= base_url('public/css/app.css') ?>" >

</head>
<body>
	
	
	<div id="app">
		<?= $topNav ?>
		<search-modal ref="searchModal"></search-modal>
		<div class="flex container mx-auto mb-4">
			<?= $sideNav ?>

			<main class="flex-1">
				<div class="lg:row">
					<div class="lg:w-1/2">
						<div class="w-4/5">
							<!-- authentication failed message -->
							<?php if($authenticationFailed) : ?>
								<disappear>
									<div class="bg-red-lightest border border-red-light text-red-dark px-4 py-3 rounded relative mb-8">
									  <strong class="font-bold">Authentication failed!</strong>
									  <span class="block sm:inline"><?= $authenticationFailed ?>.</span>
									</div>	
								</disappear>
							<?php endif ?>
							

							<!-- Form group Information -->
							<div class="mb-6">
								<p class="font-bold text-xl">Login</p>
								<p class="mt-6 text-grey-darkest">Please enter your email and password:</p>
							</div>
							<login-form></login-form>
						</div>
					</div>

					<div class="lg:w-1/2">
						<div class="w-4/5">
							<!-- registration failed message -->
							<?php if($registrationFailed) : ?>
								<disappear>
									<div class="bg-red-lightest border border-red-light text-red-dark px-4 py-3 rounded relative mb-8">
									  <strong class="font-bold">Registration failed!</strong>
									  <span class="block sm:inline"><?= $registrationFailed ?>.</span>
									</div>	
								</disappear>
							<?php endif ?>

							<!-- Form group Information -->
							<div class="mb-6">
								<p class="font-bold text-xl">Register</p>
								<p class="mt-6 text-grey-darkest">Please enter the following data:</p>
							</div>
							<register-form></register-form>
						</div>
					</div>
				</div>
			</main>
		</div>
	</div>
	

	<?= $newsletter ?>

	<?= $footer ?>
	<script src="<?= base_url('public/js/app.js') ?>"></script>
</body>
</html>