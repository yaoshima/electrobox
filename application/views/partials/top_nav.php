<header>
	<div class="flex items-center justify-end container relative mx-auto p-2 h-32">
		<!-- brand -->
		<!-- <a href="<?= site_url('/') ?>" class="absolute text-2xl uppercase no-underline" style="left: 50%; transform: translate(-50%,0);" >Electrobox</a> -->
		<a v-if="!isCartEmpty || !inCartPage" href="<?= site_url('/') ?>" class="absolute w-48" style="left: 50%; transform: translate(-50%,0);">
			<img class="w-full" src="<?= base_url('public/img/electrobox_clean.png' ) ?>" >
		</a>
		
		<!-- top nav -->
		<nav>
			<a href="<?= site_url('account') ?>"><img class="h-4 w-4 m-2" src="https://cdn.shopify.com/s/files/1/0088/4005/2836/t/8/assets/user.svg?81" alt=""></a>
			<a @click="openSearchModal" href="#"><img class="h-4 w-4 m-2" src="https://cdn.shopify.com/s/files/1/0088/4005/2836/t/8/assets/search.svg?81" alt=""></a>
			<a href="<?= site_url('/product/cart') ?>"><img class="h-4 w-4 m-2" src="https://cdn.shopify.com/s/files/1/0088/4005/2836/t/8/assets/bag.svg?81" alt=""></a>

			<a class="hidden" :style="!isEmptyObject(currentUser) ? 'display : inline' : '' " href="<?= site_url('/account/logout') ?>">
				<!-- <i class="material-icons " style="font-size: 20px; transform: translate(0,-23%);">
				power_settings_new
				</i> -->
				<img class="h-4 w-4 m-2" src="<?= base_url('public/img/logout.png') ?>" alt="">
			</a>
			
			
		</nav>
	</div>
</header>