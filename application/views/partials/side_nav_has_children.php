<!-- side nav -->
<nav class="side-nav">
	<ul>
		<li>
			<a href="<?= site_url('/product') ?>">Shop</a>
			<ul class="dropdown-menu">
				<?php foreach($categories as $category) : ?>
					<li><a href="<?= site_url('/product?category=' .$category['name'] ) ?>"><?= ucfirst(strtolower($category['name'])) ?></a></li>
				<?php endforeach ?>	
				<!-- <li><a href="#">Battery</a></li>
				<li><a href="#">Tripod</a></li>
				<li><a href="#">Camera</a></li>
				<li><a href="#">Misc</a></li> -->

			</ul>
		</li>
		<li ><a href="#">Stories</a></li>
		<li ><a href="#">About</a></li>
	</ul>
</nav>