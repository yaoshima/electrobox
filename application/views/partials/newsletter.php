<section class="bg-teal-lightest">
	<div class="container mx-auto">
		<!-- newsletter -->
		<div class="px-10 md:px-0 md:w-2/5 mx-auto py-20">
			<header class="section-header">
				<span class="line"></span>
				<span>NEWSLETTER SIGN-UP</span>
			</header>
			<p class="pt-8 text-sm text-grey-darker">Sign up to hear about the latest news and stories from Electrobox</p>
			<form class="pt-8" action="" method="POST">
				<div class="flex flex-wrap">
					<input class="flex-1 py-2 mr-4 bg-transparent border-b-2 focus:outline-none" type="text" name="email" placeholder="Email Address">
					<button class="mt-4 w-full lg:mt-0 lg:w-1/3 primary-btn">SIGN UP</button>
				</div>
				
			</form>
		</div>
	</div>
</section>