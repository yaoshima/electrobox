<!-- brand -->
<a href="<?= site_url('/') ?>" class="text-2xl uppercase no-underline">Electrobox</a>
<!-- breadcumb -->
<nav>
	<ul class="list-reset flex -mx-2 mt-2">
		<li class="px-2 flex items-center">
			<a href="#">Cart</a>
			<i class="material-icons">
			chevron_right
			</i>
		</li>
		<li class="px-2 flex items-center">
			<a href="#">Information</a>
			<i class="material-icons">
			chevron_right
			</i>
		</li>
		<li class="px-2 flex items-center">
			<a href="#">Shipping</a>
			<i class="material-icons">
			chevron_right
			</i>
		</li>
		<li class="px-2 flex items-center">
			<a href="#">Payment</a>
		</li>
	</ul>
</nav>