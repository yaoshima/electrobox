<footer class="mb-8">
	<div class="container mx-auto">
		<!-- site map -->
		<div class="row justify-between w-3/5 mx-auto py-20">
			<div>
				<!-- nav title -->
				<p class="pb-4">INFORMATION</p>
				<nav>
					<ul class="list-reset leading-normal">
						<li ><a href="#">FAQs</a></li>
						<li ><a href="#">About</a></li>
						<li ><a href="#">Shipping, Delivery &amp; Returns</a></li>
					</ul>
				</nav>
				
			</div>
			<div>
				<!-- nav title -->
				<p class="pb-4">LEGAL AREA</p>
				<nav>
					<ul class="list-reset leading-normal">
						<li ><a href="#">Terms &amp; Conditions</a></li>
						<li ><a href="#">Privacy &amp; Cookie Policy</a></li>
					</ul>
				</nav>
				
			</div>
			<div>
				<!-- nav title -->
				<p class="pb-4">FOLLOW US</p>
				<nav>
					<ul class="list-reset leading-normal">
						<li ><a href="#">Instagram</a></li>
						<li ><a href="#">Facebook</a></li>
						<li ><a href="#">Line</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<hr class="divider">

		<!-- copyright -->
		<p class="mt-6 text-xs">&copy; Electrobox 2019. All Rights Reserved.</p>
	</div>
</footer>