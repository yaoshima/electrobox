<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->library('session');
	}

	public function store(){
		$data = json_decode(file_get_contents('php://input'),true);
		$response = $this->cart->insert($data);
		echo json_encode($response);
	}

	public function getUserCart($userId){
		$response = $this->cart->userCart($userId);
		echo json_encode($response);
	}

	public function update(){
		$data = json_decode(file_get_contents('php://input'),true);
		$response = $this->cart->update($data);
		echo json_encode($response);
		
	}

	public function delete(){
		$data = json_decode(file_get_contents('php://input'),true);
		$response = $this->cart->delete($data);
		echo json_encode($response);
		
	}

}
