<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function index()
	{	
		// loading categories
		$data['categories'] = $this->category->index();

		$data['currentCategory'] = $this->input->get('category');
		$data['searchQuery'] = $this->input->get('search');
		if($data['currentCategory']){
			$category = $this->category->getByName($data['currentCategory']);
			if($category){
				$data['products'] =  $this->product->getByCategory($category['id']);
			}
			else{
				$data['products'] =  $this->product->index();
				$data['currentCategory'] = 'ALL';
			}
		}
		else if($data['searchQuery']){
			$data['products'] =  $this->product->getLikeName($data['searchQuery']);
		}
		else{
			$data['products'] =  $this->product->index();
			$data['currentCategory'] = 'ALL';
		}

		$toNavData['categories'] = $data['categories'];
		$toNavData['currentCategory'] = $data['currentCategory'];

		// loading view
		$data['topNav'] = $this->load->view('partials/top_nav.php', NULL, TRUE);
		$data['sideNavHasChildren'] = $this->load->view('partials/side_nav_has_children.php', $toNavData, TRUE);
		$data['footer'] = $this->load->view('partials/footer.php', NULL, TRUE);
		$data['newsletter'] = $this->load->view('partials/newsletter.php', NULL, TRUE);

		
		


		$this->load->view('product/index', $data);
	}

	public function show($id){
		// loading view
		$data['topNav'] = $this->load->view('partials/top_nav.php', NULL, TRUE);
		$data['sideNav'] = $this->load->view('partials/side_nav.php', NULL, TRUE);
		$data['footer'] = $this->load->view('partials/footer.php', NULL, TRUE);
		$data['newsletter'] = $this->load->view('partials/newsletter.php', NULL, TRUE);

		// loading products
		$data['product'] =  $this->product->show($id);


		$this->load->view('product/show', $data);
	}

	public function cart(){
		// loading view
		$data['topNav'] = $this->load->view('partials/top_nav.php', NULL, TRUE);
		$data['footer'] = $this->load->view('partials/footer.php', NULL, TRUE);
		$data['newsletter'] = $this->load->view('partials/newsletter.php', NULL, TRUE);


		$this->load->view('cart', $data);
	}
}
