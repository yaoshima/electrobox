<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	public function information()
	{	
		// loading view
		$data['header'] = $this->load->view('partials/checkout_header.php', NULL, TRUE);
		$data['checkoutItems'] = $this->load->view('partials/checkout_items.php', NULL, TRUE);
		$data['footer'] = $this->load->view('partials/checkout_footer.php', NULL, TRUE);

		$this->load->view('checkout/information',$data);
	}

	public function shipping()
	{	

		// loading view
		$data['header'] = $this->load->view('partials/checkout_header.php', NULL, TRUE);
		$data['checkoutItems'] = $this->load->view('partials/checkout_items.php', NULL, TRUE);
		$data['footer'] = $this->load->view('partials/checkout_footer.php', NULL, TRUE);
		
		$this->load->view('checkout/shipping',$data);
	}

	public function payment()
	{	

		// loading view
		$data['header'] = $this->load->view('partials/checkout_header.php', NULL, TRUE);
		$data['checkoutItems'] = $this->load->view('partials/checkout_items.php', NULL, TRUE);
		$data['footer'] = $this->load->view('partials/checkout_footer.php', NULL, TRUE);
		
		$this->load->view('checkout/payment',$data);
	}

	public function confirm()
	{	

		// loading view
		$data['header'] = $this->load->view('partials/checkout_header.php', NULL, TRUE);
		$data['checkoutItems'] = $this->load->view('partials/checkout_items.php', NULL, TRUE);
		$data['footer'] = $this->load->view('partials/checkout_footer.php', NULL, TRUE);
		
		$this->load->view('checkout/confirm',$data);
	}


}
