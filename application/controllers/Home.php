<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{	
		// loading view
		$data['topNav'] = $this->load->view('partials/top_nav.php', NULL, TRUE);
		$data['sideNav'] = $this->load->view('partials/side_nav.php', NULL, TRUE);
		$data['footer'] = $this->load->view('partials/footer.php', NULL, TRUE);
		$data['newsletter'] = $this->load->view('partials/newsletter.php', NULL, TRUE);


		$this->load->view('home', $data);
	}
}
