<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->library('session');
	}

	public function storeRegisterTx(){
		$data = json_decode(file_get_contents('php://input'),true);
		$response = $this->transaction->storeRegisteredUserTx($data);
		echo json_encode($response);
	}

	public function storeUnRegisterTx(){
		$data = json_decode(file_get_contents('php://input'),true);
		$response = $this->transaction->storeUnRegisteredUserTx($data);
		echo json_encode($response);
	}

	public function getUserTransaction($userId){
		$response = $this->transaction->userTransaction($userId);
		echo json_encode($response);
	}

	// public function update(){
	// 	$data = json_decode(file_get_contents('php://input'),true);
	// 	$response = $this->cart->update($data);
	// 	echo json_encode($response);
		
	// }

	// public function delete(){
	// 	$data = json_decode(file_get_contents('php://input'),true);
	// 	$response = $this->cart->delete($data);
	// 	echo json_encode($response);
		
	// }

}
