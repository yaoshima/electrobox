<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
	}

	public function index()
	{	
		if($this->session->has_userdata('currentUser')){
			redirect('account/profile');
		}

		// load error msg if any
		$data['authenticationFailed'] = $this->session->flashdata('authenticationFailed');
		$data['registrationFailed'] = $this->session->flashdata('registrationFailed');

		// loading view
		$data['topNav'] = $this->load->view('partials/top_nav.php', NULL, TRUE);
		$data['sideNav'] = $this->load->view('partials/side_nav.php', NULL, TRUE);
		$data['footer'] = $this->load->view('partials/footer.php', NULL, TRUE);
		$data['newsletter'] = $this->load->view('partials/newsletter.php', NULL, TRUE);


		$this->load->view('login', $data);
	}

	public function logout(){
		$this->session->unset_userdata('currentUser');
		redirect('account/login');
	}

	public function profile()
	{	
		
		$currentUser = $this->session->userdata('currentUser');
		if($currentUser){
			// load session msg if any
			$data['successMsg'] = $this->session->flashdata('successMsg');
			$data['failedMsg'] = $this->session->flashdata('failedMsg');

			// loading user related data
			$data['currentUser'] = $currentUser;
			$data['addresses'] = $this->address->userAddress($currentUser['id']);

			// loading view
			$data['topNav'] = $this->load->view('partials/top_nav.php', NULL, TRUE);
			$data['sideNav'] = $this->load->view('partials/side_nav.php', NULL, TRUE);
			$data['footer'] = $this->load->view('partials/footer.php', NULL, TRUE);
			$data['newsletter'] = $this->load->view('partials/newsletter.php', NULL, TRUE);


			$this->load->view('profile', $data);
		}
		else {
			redirect('account');
		}

		
	}

	public function login()
	{	
		// user get redirect if they dont submit the form
		if(!$_POST){
			redirect('account');
		}

		// get the authenticated user
		$authenticateUser =  $this->user->authenticate($_POST);

		// store it to the session and redirect to profile page
		if($authenticateUser){
			$this->session->set_userdata('currentUser', $authenticateUser);
			redirect('account/profile');
		}
		else{
			$this->session->set_flashdata('authenticationFailed', 'Your Email or Password doesn\'t match our record!');
			redirect('account');
		}
	}

	public function register(){
		// user get redirect if they dont submit the form
		if(!$_POST){
			redirect('account');
		}

		$response = $this->user->register($_POST);
		if($response['status']){
			$this->session->set_userdata('currentUser', $response['currentUser']);
			redirect('account/profile');
		}
		else{
			$this->session->set_flashdata('registrationFailed', $response['message']);
			redirect('account');
		}

	}

	public function getCurrentUser(){
		echo $this->session->userdata('currentUser')  ?  json_encode($this->session->userdata('currentUser')) : "{}";
	}
}
