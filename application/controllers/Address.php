<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->library('session');
	}

	public function store(){
		if($_POST){
			$_POST['user_id'] = $this->session->userdata('currentUser')['id'];
			if($this->address->insert($_POST)){
				$this->session->set_flashdata('successMsg', 'Your new address have been added!');
			}
			else{
				$this->session->set_flashdata('failedMsg', 'Sorry, No Address have been added!');
			}
		}
		redirect('account/profile');

	}

	public function delete($id){
		$currentUser = $this->session->userdata('currentUser');

		if($this->address->delete($currentUser['id'],$id)){
			$this->session->set_flashdata('successMsg', 'Your Address have been deleted!');
		}
		else{
			$this->session->set_flashdata('failedMsg', 'Sorry, No Address have been deleted!');
		}

		redirect('account/profile');

	}

	public function update($id){
		$currentUser = $this->session->userdata('currentUser');

		if($_POST){
			if($this->address->update($currentUser['id'],$_POST)){
				$this->session->set_flashdata('successMsg', 'Your Address have been updated!');
			}
			else{
				$this->session->set_flashdata('failedMsg', 'Sorry, No Address have been updated!');
			}
		}
		

		redirect('account/profile');

		

	}

	public function getUserAddress($id){
		$response = $this->address->userAddress($id);
		echo json_encode($response);
	}

}
