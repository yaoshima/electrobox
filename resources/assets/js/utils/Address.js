export class Address {
	static getProvince(){
		return [
			'DKI Jakarta',
			'Banten'
		]
	}
	static getCity(province){
		switch(province) {

		    case 'DKI Jakarta':
		        return [
		            'Jakarta Barat',
		            'Jakarta Utara'
		        ]
		        break;
		    case 'Banten':
		        return [
		            'Kabupaten Tangerang',
		            'Kota Tangerang'
		        ]
		        break;
		    default :
		    	return []
		}
	}
	static getSubdistrict(city){
		switch(city) {

		    case 'Jakarta Barat':
		        return [
		            'Cengkareng',
		            'Grogol Petamburan',
		            'Kalideres'
		        ]
		        break;
		    case 'Jakarta Utara':
		        return [
		            'Cilincing',
		            'Kelapa Gading',
		            'Koja'
		        ]
		        break;
		    case 'Kabupaten Tangerang':
		        return [
		            'Kelapa Dua',
		            'Mauk',
		            'Pasar Kemis'
		        ]
		        break;
		    case 'Kota Tangerang':
		        return [
		            'Karawaci',
		            'Batuceper',
		            'Karang Tengah'
		        ]
		        break;
		    default :
		    	return []
		}
	}
}