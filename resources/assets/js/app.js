import axios from 'axios';
import Vue from 'vue';
import Vuelidate from 'vuelidate'
import AccordionItem from './components/AccordionItem.vue';
import Accordion from './components/Accordion.vue';
import OverlayCart from './components/OverlayCart.vue';
import CollapsibleOrder from './components/CollapsibleOrder.vue';
import CheckoutItem from './components/CheckoutItem.vue';
import InformationForm from './components/InformationForm.vue';
import ShippingForm from './components/ShippingForm.vue';
import PaymentForm from './components/PaymentForm.vue';
import Cart from './components/Cart.vue';
import AddressModal from './components/AddressModal.vue';
import LoginForm from './components/LoginForm.vue';
import RegisterForm from './components/RegisterForm.vue';
import Disappear from './components/Disappear.vue';
import OrderHistory from './components/OrderHistory.vue';
import GeneralModal from './components/GeneralModal.vue';
import SearchModal from './components/SearchModal.vue';

const BASE_LINK = window.location.origin ;


Vue.use(Vuelidate)
Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 0
    });
    return formatter.format(value);
});

new Vue({
	el : '#app',
	components : {
		AccordionItem,
		Accordion,
		OverlayCart,
		CollapsibleOrder,
		CheckoutItem,
		InformationForm,
		ShippingForm,
		PaymentForm,
		Cart,
		AddressModal,
		LoginForm,
		RegisterForm,
		Disappear,
		OrderHistory,
		GeneralModal,
		SearchModal
	},
	mounted() {
		// axios.get('http://localhost:8001/api/product')
		// .then((response) => {
		// 	console.log(response)
		// })

		// axios.get('http://localhost:8001/api/product', {
		// 	params : {
		// 		'id': 4
		// 	}
		// })
		// .then((response) => {
		// 	console.log(response)
		// })


		// axios.post('http://localhost:8001/api/product',{
		// 	'nama' : 'canon999',
		// 	'id_kategori' : 2,
		// 	'detail' : 'apa aja',
		// 	'brand' : 'canon',           
		// 	'harga' : 9999,
		// 	'stock' : 555,
		// 	'images_name' : 'canon.png'
		// })
		// .then((response) => {
		// 	console.log(response)
		// })

		// axios.put('http://localhost:8001/api/product',{
		// 	'id' : 1,
		// 	'nama' : 'xxx',
		// 	'id_kategori' : 2,
		// 	'detail' : 'gantiya',
		// 	'brand' : 'gantiya',           
		// 	'harga' : 9999,
		// 	'stock' : 555,
		// 	'images_name' : 'gantiya.png'
		// })
		// .then((response) => {
		// 	console.log(response)
		// })

		// axios.delete('http://localhost:8001/api/product',{
		// 	data : {
		// 		'id' : 54
		// 	}
		// })
		// .then((response) => {
		// 	console.log(response)
		// })

		this.$on('emptyCart',() => {
		    this.isCartEmpty = true

		})

		this.$on('openNoAddressModal',() => {
		    this.$refs.no_address_modal.toggleModal();

		})

		axios.get(BASE_LINK + '/index.php/user/current')
		.then((response) => {
		    if(!this.isEmptyObject(response.data)){
		    	this.currentUser = response.data
		    	axios.get(BASE_LINK + '/index.php/cart/' + response.data.id)
		    	.then((response) => {
		    		this.isCartEmpty = response.data.length == 0
		    	})
		    }
		    else{
		    	this.isCartEmpty = this.isEmptyObject(this.getLocalStorage('cartItems')) 
		    }
		})


		

	},
	methods : {
		isEmptyObject(obj){
			return Object.keys(obj).length === 0 && obj.constructor === Object
		},
		openAddressModal(e,address){
			this.$refs.addressModal.toggleModal(e,address);
		},
		openSearchModal(){
			this.$refs.searchModal.toggleModal();
		},
		addToCart(product){
			product.qty = 1;

			this.$refs.overlayCart.addToCart(product);
		},
		clearLocalStorage(){
	    	localStorage.removeItem('customerInfo')
            localStorage.removeItem('cartItems')
	    },
	    getLocalStorage(key){
	    	if (localStorage.getItem(key)) {
              try {
                return JSON.parse(localStorage.getItem(key));
              } catch(e) {
                localStorage.removeItem(key);
              }
            }
            return {}
	    }
	},
	data : {
		isCartEmpty : null,
		currentUser : {}
	},
	computed : {
	    customerInfoGeneral(){
         	return this.getLocalStorage('customerInfo') 

	    },
	    inCartPage(){
	    	//console.log(window.location.pathname.split("/").slice(-1)[0])
	    	return window.location.pathname.split("/").slice(-1)[0] == 'cart'
	    }

	},
})