CREATE TABLE users (
	id int(11) NOT NULL AUTO_INCREMENT,
	email varchar(40) NOT NULL UNIQUE,
	password varchar(40) NOT NULL,
	firstname varchar(40) NOT NULL,
	lastname varchar(40) NOT NULL,

	PRIMARY KEY (id)
);

INSERT INTO users (email, password, firstname, lastname) VALUES
('yaoshima@gmail.com', '12345678', 'yao', 'shima'),
('richard@gmail.com', '12345678', 'richard', 'laurence');



CREATE TABLE addresses (
	id int(11) NOT NULL AUTO_INCREMENT,
	user_id int(11),
	email varchar(40),
	firstname varchar(40) NOT NULL,
	lastname varchar(40) NOT NULL,
	phone varchar(15) NOT NULL,
	address varchar(255) NOT NULL,
	province varchar(50) NOT NULL,
	city varchar(50) NOT NULL,
	sub_district varchar(50) NOT NULL,
	postal_code varchar(10) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (user_id) REFERENCES users(id)
);

INSERT INTO addresses (user_id, firstname, lastname, phone, address, province, city, sub_district, postal_code) VALUES
(1,'james', 'roberto', '08122312151', 'Jl Poncol Raya', 'DKI Jakarta', 'Jakarta Pusat', 'Senen', '10521');



CREATE TABLE categories (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO categories (name) VALUES
('TRIPOD'),
('BATTERY'),
('CAMERA'),
('MISC');


CREATE TABLE products (
  id int(11) NOT NULL AUTO_INCREMENT,
  category_id int(11) NOT NULL,
  name varchar(100) NOT NULL,
  brand varchar(50) NOT NULL,
  description varchar(255) NOT NULL,
  specification varchar(255) NOT NULL,
  price int(11) NOT NULL,
  stock int(11) NOT NULL,
  image varchar(100) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (category_id) REFERENCES categories(id)
);

INSERT INTO products (category_id, name, brand, description, specification, price, stock, image) VALUES
(
	3,
	'POWERSHOT SX430 IS Kamera Digital', 
	'Canon', 
	'Canon PowerShot SX430 IS Canon PowerShot SX430 IS dengan mudah menangkap detail cantik dengan kamera digital mini yang punya 45x zoom serta mudah dibawa kemana saja.',
	'Bagikan setiap moment bahagia anda bersama teman dengan smart deviceAnda sebagai remote yang mudah untuk menggunakan aplikasi Canon Connect. Cukup hubungkan hanya dengan sekali tekan menggunakan Wi-Fi dan NFC.',
	2500000,
	10,
	'canon1.jpg'
),
(
  3,
  'CANON EOS M100/CANON M100 Kit 15-45mm Paket 32gb DAHSYAT !! - Hitam', 
  'Canon', 
  'Canon EOS M100 Kit 15-45mm Kamera Mirrorless - White, kamera digital yang ringan dan compact dengan built-in Wi-Fi, NFC, dan Bluetooth, kamera 24.2MP APS-C sehingga memudahkan Anda berbagi gambar dan video segera setelah Anda memakainya.',
  'Kamera digital 24.2MP APS-C CMOS',
  5500000,
  10,
  'canon2.jpg'
),
(
  1,
  'Godric Spider Mini Tripod w/ Tripod Mount for Xiaomi Yi, BRICA & GoPro - Biru', 
  'Xiaomi', 
  'Godric Spider Mini Flexible Tripod with Tripod Mount Adapter for Xiaomi Yi, BRICA & GoPromerupakan mini tripod portable yang memiliki konstruksi fleksibel sehingga dapat membantu Anda mengambil gambar dari sudut yang sulit sekalipun.',
  'Complete flexibility, made of hard plastic',
  19000,
  10,
  'tripod1.jpg'
),
(
  4,
  'Sony SEL 18-105mm f/4 PZ G OSS Lens - Hitam', 
  'Sony', 
  'The first Sony G Lens created specifically for E-mount cameras is true to its heritage, achieving outstanding resolution and contrast throughout the zoom range thanks to an advanced optical design incorporating two ED glass elements and three aspherical elements',
  'Item stok berjalan, mohon konfirmasi dahulu sebelum melakukan transaksi terima kasih',
  5250000,
  10,
  'accessories1.png'
),
(
  2,
  'BATTERY SONY NP-FV100 - BATERAI NP FV-100 - BATRE KAMERA HANDYCAM', 
  'Sony', 
  'BATTERY SONY NP-FV100 GARANSI 1 BULAN COMPETIBEL UNTUK SONY Yang berbaik hati memberikan bintang 5, Kami doa kan semoga rejeki nya lancar, salalu berbahagia dan selalu sehat, amin :)',
  'Untuk Type di luar keterangan, harap di tanyakan terlebih dahulu, agar tidak ada kesalahan pembelian',
  230000,
  10,
  'battery1.jpg'
),
(
	3,
	'Kamera Nikon Coolpix B500', 
	'Nikon', 
	'Nikon Coolpix B500 Kamera Pocket - HItam, hadir dengan kemampuan dapat merekam video full HD. Lensanya dapat melakukan zoom hingga 40x atau setara 22,5-900mm F3.0-6.5. Sensornya BSI CMOS 16 MP dan juga memiliki layar belakang 3 inci yang dapat ditekuk. Nikon Coolpix B500 Kamera Digital ditenagai empat baterai AA sebagai sumber dayanya.',
	'ISO 6400, 7.4 fps Continuous Shooting6GB ULTRA CLASS 10',
	4500000,
	5,
	'nikon1.jpg'
);



CREATE TABLE cart (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11),
  product_id int(11) NOT NULL,
  qty int(11) NOT NULL,
  subtotal int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (product_id) REFERENCES products(id)
);

CREATE TABLE transactions (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11),
  address_id int(11),
  payment_method varchar(100) NOT NULL,
  shipping_method varchar(100) NOT NULL,
  status varchar(100) NOT NULL,
  shipping_fee int(11) NOT NULL,
  item_count int(11) NOT NULL,
  total int(11) NOT NULL,
  created_at timestamp,

  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (address_id) REFERENCES addresses(id)
);

CREATE TABLE transactions_detail (
  id int(11) NOT NULL AUTO_INCREMENT,
  transaction_id int(11),
  product_id int(11) NOT NULL,
  qty int(11) NOT NULL,
  subtotal int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (transaction_id) REFERENCES transactions(id),
  FOREIGN KEY (product_id) REFERENCES products(id)

);